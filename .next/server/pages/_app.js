module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap/dist/css/bootstrap.css":
/*!*******************************************************!*\
  !*** ./node_modules/bootstrap/dist/css/bootstrap.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/globals.css */ "./styles/globals.css");
/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/bootstrap.min.css */ "./styles/bootstrap.min.css");
/* harmony import */ var _styles_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ "./node_modules/bootstrap/dist/css/bootstrap.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _redux_store_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../redux/store.js */ "./redux/store.js");
/* harmony import */ var _src_services_authContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../src/services/authContext */ "./src/services/authContext.js");

var _jsxFileName = "/home/vndkfz/Code/ch10frontend/pages/_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const MyApp = ({
  Component,
  pageProps
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_src_services_authContext__WEBPACK_IMPORTED_MODULE_6__["AuthProvider"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Component, _objectSpread({}, pageProps), void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 9
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 12,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (_redux_store_js__WEBPACK_IMPORTED_MODULE_5__["wrapper"].withRedux(MyApp));

/***/ }),

/***/ "./redux/constants/counterConstants.js":
/*!*********************************************!*\
  !*** ./redux/constants/counterConstants.js ***!
  \*********************************************/
/*! exports provided: INCREMENT_COUNTER, DECREMENT_COUNTER */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INCREMENT_COUNTER", function() { return INCREMENT_COUNTER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DECREMENT_COUNTER", function() { return DECREMENT_COUNTER; });
//Action Types
const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
const DECREMENT_COUNTER = 'DECREMENT_COUNTER';

/***/ }),

/***/ "./redux/constants/editprofileConstants.js":
/*!*************************************************!*\
  !*** ./redux/constants/editprofileConstants.js ***!
  \*************************************************/
/*! exports provided: USER_EDIT_REQUEST, USER_EDIT_UPDATED, USER_EDIT_FAILED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_EDIT_REQUEST", function() { return USER_EDIT_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_EDIT_UPDATED", function() { return USER_EDIT_UPDATED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_EDIT_FAILED", function() { return USER_EDIT_FAILED; });
const USER_EDIT_REQUEST = 'USER_EDIT_REQUEST';
const USER_EDIT_UPDATED = 'USER_EDIT_UPDATED';
const USER_EDIT_FAILED = 'USER_EDIT_FAILED';

/***/ }),

/***/ "./redux/constants/historyConstants.js":
/*!*********************************************!*\
  !*** ./redux/constants/historyConstants.js ***!
  \*********************************************/
/*! exports provided: USER_HISTORY_REQUEST */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_HISTORY_REQUEST", function() { return USER_HISTORY_REQUEST; });
const USER_HISTORY_REQUEST = 'USER_HISTORY_REQUEST';

/***/ }),

/***/ "./redux/constants/loginConstants.js":
/*!*******************************************!*\
  !*** ./redux/constants/loginConstants.js ***!
  \*******************************************/
/*! exports provided: LOGIN_REQUEST */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGIN_REQUEST", function() { return LOGIN_REQUEST; });
const LOGIN_REQUEST = 'LOGIN_REQUEST';

/***/ }),

/***/ "./redux/constants/profileConstants.js":
/*!*********************************************!*\
  !*** ./redux/constants/profileConstants.js ***!
  \*********************************************/
/*! exports provided: USER_PROFILE_REQUEST, USER_HISTORY_REQUEST, USER_EDIT_REQUEST */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_PROFILE_REQUEST", function() { return USER_PROFILE_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_HISTORY_REQUEST", function() { return USER_HISTORY_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_EDIT_REQUEST", function() { return USER_EDIT_REQUEST; });
const USER_PROFILE_REQUEST = 'USER_PROFILE_REQUEST';
const USER_HISTORY_REQUEST = 'USER_HISTORY_REQUEST';
const USER_EDIT_REQUEST = 'USER_EDIT_REQUEST';

/***/ }),

/***/ "./redux/constants/userConstants.js":
/*!******************************************!*\
  !*** ./redux/constants/userConstants.js ***!
  \******************************************/
/*! exports provided: USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, USER_REGISTER_FAILED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_REGISTER_REQUEST", function() { return USER_REGISTER_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_REGISTER_SUCCESS", function() { return USER_REGISTER_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_REGISTER_FAILED", function() { return USER_REGISTER_FAILED; });
const USER_REGISTER_REQUEST = 'USER_REGISTER_REQUEST';
const USER_REGISTER_SUCCESS = 'USER_REGISTER_SUCCESS';
const USER_REGISTER_FAILED = 'USER_REGISTER_FAILED';

/***/ }),

/***/ "./redux/reducers/counterReducer.js":
/*!******************************************!*\
  !*** ./redux/reducers/counterReducer.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants_counterConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/counterConstants */ "./redux/constants/counterConstants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const counterReducer = (state = {
  value: 0
}, action) => {
  switch (action.type) {
    case _constants_counterConstants__WEBPACK_IMPORTED_MODULE_0__["INCREMENT_COUNTER"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        value: state.value + 1
      });

    case _constants_counterConstants__WEBPACK_IMPORTED_MODULE_0__["DECREMENT_COUNTER"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        value: state.value - 1
      });

    default:
      return _objectSpread({}, state);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (counterReducer);

/***/ }),

/***/ "./redux/reducers/editprofileReducer.js":
/*!**********************************************!*\
  !*** ./redux/reducers/editprofileReducer.js ***!
  \**********************************************/
/*! exports provided: editprofileReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editprofileReducer", function() { return editprofileReducer; });
/* harmony import */ var _constants_editprofileConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/editprofileConstants */ "./redux/constants/editprofileConstants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const editprofileReducer = (state = {}, action) => {
  switch (action.type) {
    case _constants_editprofileConstants__WEBPACK_IMPORTED_MODULE_0__["USER_EDIT_REQUEST"]:
      return {
        loading: true
      };

    case _constants_editprofileConstants__WEBPACK_IMPORTED_MODULE_0__["USER_EDIT_UPDATED"]:
      return _objectSpread(_objectSpread({
        loading: false
      }, state), {}, {
        dataUserEdit: action.payload
      });

    default:
      return state;
  }
};

/***/ }),

/***/ "./redux/reducers/historyReducer.js":
/*!******************************************!*\
  !*** ./redux/reducers/historyReducer.js ***!
  \******************************************/
/*! exports provided: historyReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "historyReducer", function() { return historyReducer; });
/* harmony import */ var _constants_historyConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/historyConstants */ "./redux/constants/historyConstants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const historyReducer = (state = {}, action) => {
  switch (action.type) {
    case _constants_historyConstants__WEBPACK_IMPORTED_MODULE_0__["USER_HISTORY_REQUEST"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        history: action.payload
      });

    default:
      return state;
  }
};

/***/ }),

/***/ "./redux/reducers/leaderboardReducer.js":
/*!**********************************************!*\
  !*** ./redux/reducers/leaderboardReducer.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants_userConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/userConstants */ "./redux/constants/userConstants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const leaderboardReducer = (state = {}, action) => {
  switch (action.type) {
    case 'LEADERBOARD_ACCESS':
      return _objectSpread(_objectSpread({}, state), {}, {
        leader: action.payload
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (leaderboardReducer);

/***/ }),

/***/ "./redux/reducers/loginReducer.js":
/*!****************************************!*\
  !*** ./redux/reducers/loginReducer.js ***!
  \****************************************/
/*! exports provided: loginReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginReducer", function() { return loginReducer; });
/* harmony import */ var _constants_loginConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/loginConstants */ "./redux/constants/loginConstants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const loginReducer = (state = {}, action) => {
  switch (action.type) {
    case _constants_loginConstants__WEBPACK_IMPORTED_MODULE_0__["LOGIN_REQUEST"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        accessToken: action.payload.accessToken,
        refreshToken: action.payload.refreshToken
      });

    default:
      return state;
  }
};

/***/ }),

/***/ "./redux/reducers/profileReducer.js":
/*!******************************************!*\
  !*** ./redux/reducers/profileReducer.js ***!
  \******************************************/
/*! exports provided: profileReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "profileReducer", function() { return profileReducer; });
/* harmony import */ var _constants_profileConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/profileConstants */ "./redux/constants/profileConstants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const profileReducer = (state = {}, action) => {
  switch (action.type) {
    case _constants_profileConstants__WEBPACK_IMPORTED_MODULE_0__["USER_PROFILE_REQUEST"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        profile: action.payload
      });

    default:
      return state;
  }
};

/***/ }),

/***/ "./redux/reducers/userReducer.js":
/*!***************************************!*\
  !*** ./redux/reducers/userReducer.js ***!
  \***************************************/
/*! exports provided: userRegisterReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userRegisterReducer", function() { return userRegisterReducer; });
/* harmony import */ var _constants_userConstants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/userConstants */ "./redux/constants/userConstants.js");

const userRegisterReducer = (state = {}, action) => {
  switch (action.type) {
    case _constants_userConstants__WEBPACK_IMPORTED_MODULE_0__["USER_REGISTER_REQUEST"]:
      return {
        loading: true
      };

    case _constants_userConstants__WEBPACK_IMPORTED_MODULE_0__["USER_REGISTER_SUCCESS"]:
      return {
        loading: false,
        userInfo: action.payload
      };

    case _constants_userConstants__WEBPACK_IMPORTED_MODULE_0__["USER_REGISTER_FAILED"]:
      return {
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};

/***/ }),

/***/ "./redux/store.js":
/*!************************!*\
  !*** ./redux/store.js ***!
  \************************/
/*! exports provided: wrapper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wrapper", function() { return wrapper; });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ "redux-thunk");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-devtools-extension */ "redux-devtools-extension");
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _reducers_counterReducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./reducers/counterReducer */ "./redux/reducers/counterReducer.js");
/* harmony import */ var _reducers_userReducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reducers/userReducer */ "./redux/reducers/userReducer.js");
/* harmony import */ var _reducers_leaderboardReducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reducers/leaderboardReducer */ "./redux/reducers/leaderboardReducer.js");
/* harmony import */ var _reducers_loginReducer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./reducers/loginReducer */ "./redux/reducers/loginReducer.js");
/* harmony import */ var _reducers_profileReducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./reducers/profileReducer */ "./redux/reducers/profileReducer.js");
/* harmony import */ var _reducers_historyReducer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./reducers/historyReducer */ "./redux/reducers/historyReducer.js");
/* harmony import */ var _reducers_editprofileReducer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./reducers/editprofileReducer */ "./redux/reducers/editprofileReducer.js");











const reducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  counter: _reducers_counterReducer__WEBPACK_IMPORTED_MODULE_4__["default"],
  userRegister: _reducers_userReducer__WEBPACK_IMPORTED_MODULE_5__["userRegisterReducer"],
  leaderBoard: _reducers_leaderboardReducer__WEBPACK_IMPORTED_MODULE_6__["default"],
  login: _reducers_loginReducer__WEBPACK_IMPORTED_MODULE_7__["loginReducer"],
  profileUser: _reducers_profileReducer__WEBPACK_IMPORTED_MODULE_8__["profileReducer"],
  historyProfile: _reducers_historyReducer__WEBPACK_IMPORTED_MODULE_9__["historyReducer"],
  editProfile: _reducers_editprofileReducer__WEBPACK_IMPORTED_MODULE_10__["editprofileReducer"]
});
const initialState = {};
const middleware = [redux_thunk__WEBPACK_IMPORTED_MODULE_1___default.a];

const makeStore = context => Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(reducer, initialState, Object(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__["composeWithDevTools"])(Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware)));

const wrapper = Object(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__["createWrapper"])(makeStore);

/***/ }),

/***/ "./src/services/authContext.js":
/*!*************************************!*\
  !*** ./src/services/authContext.js ***!
  \*************************************/
/*! exports provided: AuthProvider, useAuth, useIsAuthenticated */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthProvider", function() { return AuthProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useAuth", function() { return useAuth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useIsAuthenticated", function() { return useIsAuthenticated; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _authService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./authService */ "./src/services/authService.js");

var _jsxFileName = "/home/vndkfz/Code/ch10frontend/src/services/authContext.js";


const AuthContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__["createContext"])({
  isAuthenticated: false,
  setAuthenticated: () => {}
});
const AuthProvider = ({
  children
}) => {
  const {
    0: isAuthenticated,
    1: setAuthenticated
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const {
    0: isLoading,
    1: setLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true);
  const {
    0: user,
    1: setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    async function loadFromAuthService() {
      const response = await _authService__WEBPACK_IMPORTED_MODULE_2__["default"].getCurrentUser();

      if (response) {
        setUser(user);
        setAuthenticated(true);
      }

      setLoading(false);
    }

    loadFromAuthService();
  }, []);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(AuthContext.Provider, {
    value: {
      isAuthenticated,
      isLoading,
      setAuthenticated,
      user
    },
    children: children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 25,
    columnNumber: 5
  }, undefined);
};
function useAuth() {
  const context = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(AuthContext);

  if (context === undefined) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return context;
}
function useIsAuthenticated() {
  const context = useAuth();
  return context.isAuthenticated;
}

/***/ }),

/***/ "./src/services/authService.js":
/*!*************************************!*\
  !*** ./src/services/authService.js ***!
  \*************************************/
/*! exports provided: login, logout, getCurrentUser, getJwt, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "login", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logout", function() { return logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrentUser", function() { return getCurrentUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getJwt", function() { return getJwt; });
/* harmony import */ var _httpService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./httpService */ "./src/services/httpService.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jwt-decode */ "jwt-decode");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_1__);


const endPoint = `https://pacific-taiga-53059.herokuapp.com/login`;
const tokenKey = 'binar.access.token';
const refreshKey = 'binar.refresh.token'; // http.setJwt(getJwt())
// login

async function login(username, password) {
  const {
    data
  } = await _httpService__WEBPACK_IMPORTED_MODULE_0__["default"].post(endPoint, {
    username: username,
    password: password
  });
  localStorage.setItem(tokenKey, data.accessToken);
  localStorage.setItem(refreshKey, data.refreshToken);
}
function logout() {
  localStorage.removeItem(tokenKey);
  localStorage.removeItem(refreshKey);
}
function getCurrentUser() {
  try {
    const token = localStorage.getItem(tokenKey);
    return jwt_decode__WEBPACK_IMPORTED_MODULE_1___default()(token);
  } catch (err) {
    return null;
  }
}
function getJwt() {
  return localStorage.getItem(tokenKey);
} // export function getNewAccessToken() {
//   const refresh = localStorage.getItem('refreshToken')
//   const request = await http.post(config.apiUrl + 'refresh-token', {
//     refreshToken: refresh,
//   })
//   if (request) {
//     for (var k in request) {
//       localStorage.setItem(k, request[k])
//     }
//   }
// }

const auth = {
  login,
  logout,
  getCurrentUser,
  getJwt
};
/* harmony default export */ __webpack_exports__["default"] = (auth);

/***/ }),

/***/ "./src/services/httpService.js":
/*!*************************************!*\
  !*** ./src/services/httpService.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_1__);


axios__WEBPACK_IMPORTED_MODULE_0___default.a.interceptors.response.use(null, err => {
  const expectedError = err.response && err.response.status >= 400 && err.response.status < 500;

  if (!expectedError) {
    console.log("Logging the unexpected error", err);
    react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].error("An unexpected error occured.");
  }

  return Promise.reject(err);
});

function setJwt(token) {
  axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.headers.common["Authorization"] = token;
}

const http = {
  get: axios__WEBPACK_IMPORTED_MODULE_0___default.a.get,
  post: axios__WEBPACK_IMPORTED_MODULE_0___default.a.post,
  put: axios__WEBPACK_IMPORTED_MODULE_0___default.a.put,
  delete: axios__WEBPACK_IMPORTED_MODULE_0___default.a.delete,
  setJwt
};
/* harmony default export */ __webpack_exports__["default"] = (http);

/***/ }),

/***/ "./styles/bootstrap.min.css":
/*!**********************************!*\
  !*** ./styles/bootstrap.min.css ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "jwt-decode":
/*!*****************************!*\
  !*** external "jwt-decode" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jwt-decode");

/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension":
/*!*******************************************!*\
  !*** external "redux-devtools-extension" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvX2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9yZWR1eC9jb25zdGFudHMvY291bnRlckNvbnN0YW50cy5qcyIsIndlYnBhY2s6Ly8vLi9yZWR1eC9jb25zdGFudHMvZWRpdHByb2ZpbGVDb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vcmVkdXgvY29uc3RhbnRzL2hpc3RvcnlDb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vcmVkdXgvY29uc3RhbnRzL2xvZ2luQ29uc3RhbnRzLmpzIiwid2VicGFjazovLy8uL3JlZHV4L2NvbnN0YW50cy9wcm9maWxlQ29uc3RhbnRzLmpzIiwid2VicGFjazovLy8uL3JlZHV4L2NvbnN0YW50cy91c2VyQ29uc3RhbnRzLmpzIiwid2VicGFjazovLy8uL3JlZHV4L3JlZHVjZXJzL2NvdW50ZXJSZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3JlZHV4L3JlZHVjZXJzL2VkaXRwcm9maWxlUmVkdWNlci5qcyIsIndlYnBhY2s6Ly8vLi9yZWR1eC9yZWR1Y2Vycy9oaXN0b3J5UmVkdWNlci5qcyIsIndlYnBhY2s6Ly8vLi9yZWR1eC9yZWR1Y2Vycy9sZWFkZXJib2FyZFJlZHVjZXIuanMiLCJ3ZWJwYWNrOi8vLy4vcmVkdXgvcmVkdWNlcnMvbG9naW5SZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3JlZHV4L3JlZHVjZXJzL3Byb2ZpbGVSZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3JlZHV4L3JlZHVjZXJzL3VzZXJSZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3JlZHV4L3N0b3JlLmpzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2aWNlcy9hdXRoQ29udGV4dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZXMvYXV0aFNlcnZpY2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZpY2VzL2h0dHBTZXJ2aWNlLmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcImF4aW9zXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiand0LWRlY29kZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQtcmVkdXgtd3JhcHBlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtdG9hc3RpZnlcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWR1eFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlZHV4LWRldnRvb2xzLWV4dGVuc2lvblwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlZHV4LXRodW5rXCIiXSwibmFtZXMiOlsiTXlBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiLCJ3cmFwcGVyIiwid2l0aFJlZHV4IiwiSU5DUkVNRU5UX0NPVU5URVIiLCJERUNSRU1FTlRfQ09VTlRFUiIsIlVTRVJfRURJVF9SRVFVRVNUIiwiVVNFUl9FRElUX1VQREFURUQiLCJVU0VSX0VESVRfRkFJTEVEIiwiVVNFUl9ISVNUT1JZX1JFUVVFU1QiLCJMT0dJTl9SRVFVRVNUIiwiVVNFUl9QUk9GSUxFX1JFUVVFU1QiLCJVU0VSX1JFR0lTVEVSX1JFUVVFU1QiLCJVU0VSX1JFR0lTVEVSX1NVQ0NFU1MiLCJVU0VSX1JFR0lTVEVSX0ZBSUxFRCIsImNvdW50ZXJSZWR1Y2VyIiwic3RhdGUiLCJ2YWx1ZSIsImFjdGlvbiIsInR5cGUiLCJlZGl0cHJvZmlsZVJlZHVjZXIiLCJsb2FkaW5nIiwiZGF0YVVzZXJFZGl0IiwicGF5bG9hZCIsImhpc3RvcnlSZWR1Y2VyIiwiaGlzdG9yeSIsImxlYWRlcmJvYXJkUmVkdWNlciIsImxlYWRlciIsImxvZ2luUmVkdWNlciIsImFjY2Vzc1Rva2VuIiwicmVmcmVzaFRva2VuIiwicHJvZmlsZVJlZHVjZXIiLCJwcm9maWxlIiwidXNlclJlZ2lzdGVyUmVkdWNlciIsInVzZXJJbmZvIiwiZXJyb3IiLCJyZWR1Y2VyIiwiY29tYmluZVJlZHVjZXJzIiwiY291bnRlciIsInVzZXJSZWdpc3RlciIsImxlYWRlckJvYXJkIiwibG9naW4iLCJwcm9maWxlVXNlciIsImhpc3RvcnlQcm9maWxlIiwiZWRpdFByb2ZpbGUiLCJpbml0aWFsU3RhdGUiLCJtaWRkbGV3YXJlIiwidGh1bmsiLCJtYWtlU3RvcmUiLCJjb250ZXh0IiwiY3JlYXRlU3RvcmUiLCJjb21wb3NlV2l0aERldlRvb2xzIiwiYXBwbHlNaWRkbGV3YXJlIiwiY3JlYXRlV3JhcHBlciIsIkF1dGhDb250ZXh0IiwiY3JlYXRlQ29udGV4dCIsImlzQXV0aGVudGljYXRlZCIsInNldEF1dGhlbnRpY2F0ZWQiLCJBdXRoUHJvdmlkZXIiLCJjaGlsZHJlbiIsInVzZVN0YXRlIiwiaXNMb2FkaW5nIiwic2V0TG9hZGluZyIsInVzZXIiLCJzZXRVc2VyIiwidXNlRWZmZWN0IiwibG9hZEZyb21BdXRoU2VydmljZSIsInJlc3BvbnNlIiwiYXV0aCIsImdldEN1cnJlbnRVc2VyIiwidXNlQXV0aCIsInVzZUNvbnRleHQiLCJ1bmRlZmluZWQiLCJFcnJvciIsInVzZUlzQXV0aGVudGljYXRlZCIsImVuZFBvaW50IiwidG9rZW5LZXkiLCJyZWZyZXNoS2V5IiwidXNlcm5hbWUiLCJwYXNzd29yZCIsImRhdGEiLCJodHRwIiwicG9zdCIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJsb2dvdXQiLCJyZW1vdmVJdGVtIiwidG9rZW4iLCJnZXRJdGVtIiwiand0RGVjb2RlIiwiZXJyIiwiZ2V0Snd0IiwiYXhpb3MiLCJpbnRlcmNlcHRvcnMiLCJ1c2UiLCJleHBlY3RlZEVycm9yIiwic3RhdHVzIiwiY29uc29sZSIsImxvZyIsInRvYXN0IiwiUHJvbWlzZSIsInJlamVjdCIsInNldEp3dCIsImRlZmF1bHRzIiwiaGVhZGVycyIsImNvbW1vbiIsImdldCIsInB1dCIsImRlbGV0ZSJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBSUEsTUFBTUEsS0FBSyxHQUFHLENBQUM7QUFBRUMsV0FBRjtBQUFhQztBQUFiLENBQUQsS0FBOEI7QUFDMUMsc0JBQ0UscUVBQUMsc0VBQUQ7QUFBQSwyQkFDSSxxRUFBQyxTQUFELG9CQUFlQSxTQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFLRCxDQU5EOztBQVNlQyxzSEFBTyxDQUFDQyxTQUFSLENBQWtCSixLQUFsQixDQUFmLEU7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUNPLE1BQU1LLGlCQUFpQixHQUFHLG1CQUExQjtBQUNBLE1BQU1DLGlCQUFpQixHQUFHLG1CQUExQixDOzs7Ozs7Ozs7Ozs7QUNGUDtBQUFBO0FBQUE7QUFBQTtBQUFPLE1BQU1DLGlCQUFpQixHQUFHLG1CQUExQjtBQUNBLE1BQU1DLGlCQUFpQixHQUFHLG1CQUExQjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLGtCQUF6QixDOzs7Ozs7Ozs7Ozs7QUNGUDtBQUFBO0FBQU8sTUFBTUMsb0JBQW9CLEdBQUcsc0JBQTdCLEM7Ozs7Ozs7Ozs7OztBQ0FQO0FBQUE7QUFBTyxNQUFNQyxhQUFhLEdBQUcsZUFBdEIsQzs7Ozs7Ozs7Ozs7O0FDQVA7QUFBQTtBQUFBO0FBQUE7QUFBTyxNQUFNQyxvQkFBb0IsR0FBRyxzQkFBN0I7QUFDQSxNQUFNRixvQkFBb0IsR0FBRyxzQkFBN0I7QUFDQSxNQUFNSCxpQkFBaUIsR0FBRyxtQkFBMUIsQzs7Ozs7Ozs7Ozs7O0FDRlA7QUFBQTtBQUFBO0FBQUE7QUFBTyxNQUFNTSxxQkFBcUIsR0FBRyx1QkFBOUI7QUFDQSxNQUFNQyxxQkFBcUIsR0FBRyx1QkFBOUI7QUFDQSxNQUFNQyxvQkFBb0IsR0FBRyxzQkFBN0IsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGUDs7QUFLQSxNQUFNQyxjQUFjLEdBQUcsQ0FBQ0MsS0FBSyxHQUFHO0FBQUVDLE9BQUssRUFBRTtBQUFULENBQVQsRUFBdUJDLE1BQXZCLEtBQWtDO0FBQ3ZELFVBQVFBLE1BQU0sQ0FBQ0MsSUFBZjtBQUNFLFNBQUtmLDZFQUFMO0FBQ0UsNkNBQVlZLEtBQVo7QUFBbUJDLGFBQUssRUFBRUQsS0FBSyxDQUFDQyxLQUFOLEdBQWM7QUFBeEM7O0FBQ0YsU0FBS1osNkVBQUw7QUFDRSw2Q0FBWVcsS0FBWjtBQUFtQkMsYUFBSyxFQUFFRCxLQUFLLENBQUNDLEtBQU4sR0FBYztBQUF4Qzs7QUFDRjtBQUNFLCtCQUFZRCxLQUFaO0FBTko7QUFRRCxDQVREOztBQVdlRCw2RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQkE7QUFLTyxNQUFNSyxrQkFBa0IsR0FBRyxDQUFDSixLQUFLLEdBQUcsRUFBVCxFQUFhRSxNQUFiLEtBQXdCO0FBQ3hELFVBQVFBLE1BQU0sQ0FBQ0MsSUFBZjtBQUNFLFNBQUtiLGlGQUFMO0FBQ0UsYUFBTztBQUNMZSxlQUFPLEVBQUU7QUFESixPQUFQOztBQUdGLFNBQUtkLGlGQUFMO0FBQ0U7QUFDRWMsZUFBTyxFQUFFO0FBRFgsU0FDcUJMLEtBRHJCO0FBRUVNLG9CQUFZLEVBQUVKLE1BQU0sQ0FBQ0s7QUFGdkI7O0FBSUY7QUFDRSxhQUFPUCxLQUFQO0FBWEo7QUFhRCxDQWRNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0xQO0FBQ08sTUFBTVEsY0FBYyxHQUFHLENBQUNSLEtBQUssR0FBRyxFQUFULEVBQWFFLE1BQWIsS0FBd0I7QUFDcEQsVUFBUUEsTUFBTSxDQUFDQyxJQUFmO0FBQ0UsU0FBS1YsZ0ZBQUw7QUFDRSw2Q0FDS08sS0FETDtBQUVFUyxlQUFPLEVBQUVQLE1BQU0sQ0FBQ0s7QUFGbEI7O0FBSUY7QUFDRSxhQUFPUCxLQUFQO0FBUEo7QUFTRCxDQVZNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRFA7O0FBRUEsTUFBTVUsa0JBQWtCLEdBQUcsQ0FBQ1YsS0FBSyxHQUFHLEVBQVQsRUFBYUUsTUFBYixLQUF3QjtBQUNqRCxVQUFRQSxNQUFNLENBQUNDLElBQWY7QUFDRSxTQUFLLG9CQUFMO0FBQ0UsNkNBQVlILEtBQVo7QUFBbUJXLGNBQU0sRUFBRVQsTUFBTSxDQUFDSztBQUFsQzs7QUFDRjtBQUNFLGFBQU9QLEtBQVA7QUFKSjtBQU1ELENBUEQ7O0FBU2VVLGlGQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1hBO0FBRU8sTUFBTUUsWUFBWSxHQUFHLENBQUNaLEtBQUssR0FBRyxFQUFULEVBQWFFLE1BQWIsS0FBd0I7QUFDbEQsVUFBUUEsTUFBTSxDQUFDQyxJQUFmO0FBQ0UsU0FBS1QsdUVBQUw7QUFDRSw2Q0FDS00sS0FETDtBQUVFYSxtQkFBVyxFQUFFWCxNQUFNLENBQUNLLE9BQVAsQ0FBZU0sV0FGOUI7QUFHRUMsb0JBQVksRUFBRVosTUFBTSxDQUFDSyxPQUFQLENBQWVPO0FBSC9COztBQUtGO0FBQ0UsYUFBT2QsS0FBUDtBQVJKO0FBVUQsQ0FYTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGUDtBQU1PLE1BQU1lLGNBQWMsR0FBRyxDQUFDZixLQUFLLEdBQUcsRUFBVCxFQUFhRSxNQUFiLEtBQXdCO0FBQ3BELFVBQVFBLE1BQU0sQ0FBQ0MsSUFBZjtBQUNFLFNBQUtSLGdGQUFMO0FBQ0UsNkNBQ0tLLEtBREw7QUFFRWdCLGVBQU8sRUFBRWQsTUFBTSxDQUFDSztBQUZsQjs7QUFJRjtBQUNFLGFBQU9QLEtBQVA7QUFQSjtBQVNELENBVk0sQzs7Ozs7Ozs7Ozs7O0FDTlA7QUFBQTtBQUFBO0FBQUE7QUFNTyxNQUFNaUIsbUJBQW1CLEdBQUcsQ0FBQ2pCLEtBQUssR0FBRyxFQUFULEVBQWFFLE1BQWIsS0FBd0I7QUFDekQsVUFBUUEsTUFBTSxDQUFDQyxJQUFmO0FBQ0UsU0FBS1AsOEVBQUw7QUFDRSxhQUFPO0FBQUVTLGVBQU8sRUFBRTtBQUFYLE9BQVA7O0FBQ0YsU0FBS1IsOEVBQUw7QUFDRSxhQUFPO0FBQUVRLGVBQU8sRUFBRSxLQUFYO0FBQWtCYSxnQkFBUSxFQUFFaEIsTUFBTSxDQUFDSztBQUFuQyxPQUFQOztBQUNGLFNBQUtULDZFQUFMO0FBQ0UsYUFBTztBQUFFTyxlQUFPLEVBQUUsS0FBWDtBQUFrQmMsYUFBSyxFQUFFakIsTUFBTSxDQUFDSztBQUFoQyxPQUFQOztBQUNGO0FBQ0UsYUFBT1AsS0FBUDtBQVJKO0FBVUQsQ0FYTSxDOzs7Ozs7Ozs7Ozs7QUNOUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLE1BQU1vQixPQUFPLEdBQUdDLDZEQUFlLENBQUM7QUFDOUJDLFNBQU8sRUFBRXZCLGdFQURxQjtBQUU5QndCLGNBQVksRUFBRU4seUVBRmdCO0FBRzlCTyxhQUFXLEVBQUVkLG9FQUhpQjtBQUk5QmUsT0FBSyxFQUFFYixtRUFKdUI7QUFLOUJjLGFBQVcsRUFBRVgsdUVBTGlCO0FBTTlCWSxnQkFBYyxFQUFFbkIsdUVBTmM7QUFPOUJvQixhQUFXLEVBQUV4QixnRkFBa0JBO0FBUEQsQ0FBRCxDQUEvQjtBQVVBLE1BQU15QixZQUFZLEdBQUcsRUFBckI7QUFFQSxNQUFNQyxVQUFVLEdBQUcsQ0FBQ0Msa0RBQUQsQ0FBbkI7O0FBRUEsTUFBTUMsU0FBUyxHQUFJQyxPQUFELElBQ2hCQyx5REFBVyxDQUNUZCxPQURTLEVBRVRTLFlBRlMsRUFHVE0sb0ZBQW1CLENBQUNDLDZEQUFlLENBQUMsR0FBR04sVUFBSixDQUFoQixDQUhWLENBRGI7O0FBT08sTUFBTTVDLE9BQU8sR0FBR21ELHdFQUFhLENBQUNMLFNBQUQsQ0FBN0IsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQ1A7QUFDQTtBQUVBLE1BQU1NLFdBQVcsZ0JBQUdDLDJEQUFhLENBQUM7QUFDaENDLGlCQUFlLEVBQUUsS0FEZTtBQUVoQ0Msa0JBQWdCLEVBQUUsTUFBTSxDQUFFO0FBRk0sQ0FBRCxDQUFqQztBQUtPLE1BQU1DLFlBQVksR0FBRyxDQUFDO0FBQUVDO0FBQUYsQ0FBRCxLQUFrQjtBQUM1QyxRQUFNO0FBQUEsT0FBQ0gsZUFBRDtBQUFBLE9BQWtCQztBQUFsQixNQUFzQ0csc0RBQVEsQ0FBQyxLQUFELENBQXBEO0FBQ0EsUUFBTTtBQUFBLE9BQUNDLFNBQUQ7QUFBQSxPQUFZQztBQUFaLE1BQTBCRixzREFBUSxDQUFDLElBQUQsQ0FBeEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0csSUFBRDtBQUFBLE9BQU9DO0FBQVAsTUFBa0JKLHNEQUFRLENBQUMsSUFBRCxDQUFoQztBQUNBSyx5REFBUyxDQUFDLE1BQU07QUFDZCxtQkFBZUMsbUJBQWYsR0FBcUM7QUFDakMsWUFBTUMsUUFBUSxHQUFHLE1BQU1DLG9EQUFJLENBQUNDLGNBQUwsRUFBdkI7O0FBQ0EsVUFBSUYsUUFBSixFQUFjO0FBQ1ZILGVBQU8sQ0FBQ0QsSUFBRCxDQUFQO0FBQ0FOLHdCQUFnQixDQUFDLElBQUQsQ0FBaEI7QUFDSDs7QUFDREssZ0JBQVUsQ0FBQyxLQUFELENBQVY7QUFDSDs7QUFDREksdUJBQW1CO0FBQ3BCLEdBVlEsRUFVTixFQVZNLENBQVQ7QUFXQSxzQkFDRSxxRUFBQyxXQUFELENBQWEsUUFBYjtBQUNFLFNBQUssRUFBRTtBQUNMVixxQkFESztBQUVMSyxlQUZLO0FBR0xKLHNCQUhLO0FBSUxNO0FBSkssS0FEVDtBQUFBLGNBUUdKO0FBUkg7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBWUQsQ0EzQk07QUE2QkEsU0FBU1csT0FBVCxHQUFtQjtBQUN4QixRQUFNckIsT0FBTyxHQUFHc0Isd0RBQVUsQ0FBQ2pCLFdBQUQsQ0FBMUI7O0FBQ0EsTUFBSUwsT0FBTyxLQUFLdUIsU0FBaEIsRUFBMkI7QUFDekIsVUFBTSxJQUFJQyxLQUFKLENBQVUsNkNBQVYsQ0FBTjtBQUNEOztBQUNELFNBQU94QixPQUFQO0FBQ0Q7QUFFTSxTQUFTeUIsa0JBQVQsR0FBOEI7QUFDbkMsUUFBTXpCLE9BQU8sR0FBR3FCLE9BQU8sRUFBdkI7QUFDQSxTQUFPckIsT0FBTyxDQUFDTyxlQUFmO0FBQ0QsQzs7Ozs7Ozs7Ozs7O0FDaEREO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUEsTUFBTW1CLFFBQVEsR0FBSSxpREFBbEI7QUFDQSxNQUFNQyxRQUFRLEdBQUcsb0JBQWpCO0FBQ0EsTUFBTUMsVUFBVSxHQUFHLHFCQUFuQixDLENBRUE7QUFFQTs7QUFDTyxlQUFlcEMsS0FBZixDQUFxQnFDLFFBQXJCLEVBQStCQyxRQUEvQixFQUF5QztBQUM5QyxRQUFNO0FBQUVDO0FBQUYsTUFBVyxNQUFNQyxvREFBSSxDQUFDQyxJQUFMLENBQVVQLFFBQVYsRUFBb0I7QUFDekNHLFlBQVEsRUFBRUEsUUFEK0I7QUFFekNDLFlBQVEsRUFBRUE7QUFGK0IsR0FBcEIsQ0FBdkI7QUFJQUksY0FBWSxDQUFDQyxPQUFiLENBQXFCUixRQUFyQixFQUErQkksSUFBSSxDQUFDbkQsV0FBcEM7QUFDQXNELGNBQVksQ0FBQ0MsT0FBYixDQUFxQlAsVUFBckIsRUFBaUNHLElBQUksQ0FBQ2xELFlBQXRDO0FBQ0Q7QUFFTSxTQUFTdUQsTUFBVCxHQUFrQjtBQUN2QkYsY0FBWSxDQUFDRyxVQUFiLENBQXdCVixRQUF4QjtBQUNBTyxjQUFZLENBQUNHLFVBQWIsQ0FBd0JULFVBQXhCO0FBQ0Q7QUFFTSxTQUFTUixjQUFULEdBQTBCO0FBQy9CLE1BQUk7QUFDRixVQUFNa0IsS0FBSyxHQUFHSixZQUFZLENBQUNLLE9BQWIsQ0FBcUJaLFFBQXJCLENBQWQ7QUFDQSxXQUFPYSxpREFBUyxDQUFDRixLQUFELENBQWhCO0FBQ0QsR0FIRCxDQUdFLE9BQU9HLEdBQVAsRUFBWTtBQUNaLFdBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFFTSxTQUFTQyxNQUFULEdBQWtCO0FBQ3ZCLFNBQU9SLFlBQVksQ0FBQ0ssT0FBYixDQUFxQlosUUFBckIsQ0FBUDtBQUNELEMsQ0FFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1SLElBQUksR0FBRztBQUNYM0IsT0FEVztBQUVYNEMsUUFGVztBQUdYaEIsZ0JBSFc7QUFJWHNCO0FBSlcsQ0FBYjtBQU9ldkIsbUVBQWYsRTs7Ozs7Ozs7Ozs7O0FDeERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUF3Qiw0Q0FBSyxDQUFDQyxZQUFOLENBQW1CMUIsUUFBbkIsQ0FBNEIyQixHQUE1QixDQUFnQyxJQUFoQyxFQUF1Q0osR0FBRCxJQUFTO0FBQzdDLFFBQU1LLGFBQWEsR0FDakJMLEdBQUcsQ0FBQ3ZCLFFBQUosSUFBZ0J1QixHQUFHLENBQUN2QixRQUFKLENBQWE2QixNQUFiLElBQXVCLEdBQXZDLElBQThDTixHQUFHLENBQUN2QixRQUFKLENBQWE2QixNQUFiLEdBQXNCLEdBRHRFOztBQUdBLE1BQUksQ0FBQ0QsYUFBTCxFQUFvQjtBQUNsQkUsV0FBTyxDQUFDQyxHQUFSLENBQVksOEJBQVosRUFBNENSLEdBQTVDO0FBQ0FTLHdEQUFLLENBQUNoRSxLQUFOLENBQVksOEJBQVo7QUFDRDs7QUFFRCxTQUFPaUUsT0FBTyxDQUFDQyxNQUFSLENBQWVYLEdBQWYsQ0FBUDtBQUNELENBVkQ7O0FBWUEsU0FBU1ksTUFBVCxDQUFnQmYsS0FBaEIsRUFBdUI7QUFDckJLLDhDQUFLLENBQUNXLFFBQU4sQ0FBZUMsT0FBZixDQUF1QkMsTUFBdkIsQ0FBOEIsZUFBOUIsSUFBaURsQixLQUFqRDtBQUNEOztBQUVELE1BQU1OLElBQUksR0FBRztBQUNYeUIsS0FBRyxFQUFFZCw0Q0FBSyxDQUFDYyxHQURBO0FBRVh4QixNQUFJLEVBQUVVLDRDQUFLLENBQUNWLElBRkQ7QUFHWHlCLEtBQUcsRUFBRWYsNENBQUssQ0FBQ2UsR0FIQTtBQUlYQyxRQUFNLEVBQUVoQiw0Q0FBSyxDQUFDZ0IsTUFKSDtBQUtYTjtBQUxXLENBQWI7QUFRZXJCLG1FQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNCQSxrQzs7Ozs7Ozs7Ozs7QUNBQSx1Qzs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSwyQzs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSxxRDs7Ozs7Ozs7Ozs7QUNBQSx3QyIsImZpbGUiOiJwYWdlcy9fYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDApO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xuaW1wb3J0ICcuLi9zdHlsZXMvZ2xvYmFscy5jc3MnXG5pbXBvcnQgJy4uL3N0eWxlcy9ib290c3RyYXAubWluLmNzcydcbmltcG9ydCAnYm9vdHN0cmFwL2Rpc3QvY3NzL2Jvb3RzdHJhcC5jc3MnXG5pbXBvcnQgeyB3cmFwcGVyIH0gZnJvbSAnLi4vcmVkdXgvc3RvcmUuanMnXG5pbXBvcnQgeyBBdXRoUHJvdmlkZXIgfSBmcm9tICcuLi9zcmMvc2VydmljZXMvYXV0aENvbnRleHQnXG5cblxuXG5jb25zdCBNeUFwcCA9ICh7IENvbXBvbmVudCwgcGFnZVByb3BzIH0pID0+IHtcbiAgcmV0dXJuIChcbiAgICA8QXV0aFByb3ZpZGVyPlxuICAgICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XG4gICAgPC9BdXRoUHJvdmlkZXI+XG4gIClcbn1cblxuXG5leHBvcnQgZGVmYXVsdCB3cmFwcGVyLndpdGhSZWR1eChNeUFwcClcblxuIiwiLy9BY3Rpb24gVHlwZXNcbmV4cG9ydCBjb25zdCBJTkNSRU1FTlRfQ09VTlRFUiA9ICdJTkNSRU1FTlRfQ09VTlRFUidcbmV4cG9ydCBjb25zdCBERUNSRU1FTlRfQ09VTlRFUiA9ICdERUNSRU1FTlRfQ09VTlRFUidcbiIsImV4cG9ydCBjb25zdCBVU0VSX0VESVRfUkVRVUVTVCA9ICdVU0VSX0VESVRfUkVRVUVTVCdcbmV4cG9ydCBjb25zdCBVU0VSX0VESVRfVVBEQVRFRCA9ICdVU0VSX0VESVRfVVBEQVRFRCdcbmV4cG9ydCBjb25zdCBVU0VSX0VESVRfRkFJTEVEID0gJ1VTRVJfRURJVF9GQUlMRUQnIiwiZXhwb3J0IGNvbnN0IFVTRVJfSElTVE9SWV9SRVFVRVNUID0gJ1VTRVJfSElTVE9SWV9SRVFVRVNUJ1xuIiwiZXhwb3J0IGNvbnN0IExPR0lOX1JFUVVFU1QgPSAnTE9HSU5fUkVRVUVTVCc7IiwiZXhwb3J0IGNvbnN0IFVTRVJfUFJPRklMRV9SRVFVRVNUID0gJ1VTRVJfUFJPRklMRV9SRVFVRVNUJ1xuZXhwb3J0IGNvbnN0IFVTRVJfSElTVE9SWV9SRVFVRVNUID0gJ1VTRVJfSElTVE9SWV9SRVFVRVNUJ1xuZXhwb3J0IGNvbnN0IFVTRVJfRURJVF9SRVFVRVNUID0gJ1VTRVJfRURJVF9SRVFVRVNUJ1xuIiwiZXhwb3J0IGNvbnN0IFVTRVJfUkVHSVNURVJfUkVRVUVTVCA9ICdVU0VSX1JFR0lTVEVSX1JFUVVFU1QnXG5leHBvcnQgY29uc3QgVVNFUl9SRUdJU1RFUl9TVUNDRVNTID0gJ1VTRVJfUkVHSVNURVJfU1VDQ0VTUydcbmV4cG9ydCBjb25zdCBVU0VSX1JFR0lTVEVSX0ZBSUxFRCA9ICdVU0VSX1JFR0lTVEVSX0ZBSUxFRCdcbiIsImltcG9ydCB7XG4gIERFQ1JFTUVOVF9DT1VOVEVSLFxuICBJTkNSRU1FTlRfQ09VTlRFUixcbn0gZnJvbSAnLi4vY29uc3RhbnRzL2NvdW50ZXJDb25zdGFudHMnXG5cbmNvbnN0IGNvdW50ZXJSZWR1Y2VyID0gKHN0YXRlID0geyB2YWx1ZTogMCB9LCBhY3Rpb24pID0+IHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgSU5DUkVNRU5UX0NPVU5URVI6XG4gICAgICByZXR1cm4geyAuLi5zdGF0ZSwgdmFsdWU6IHN0YXRlLnZhbHVlICsgMSB9XG4gICAgY2FzZSBERUNSRU1FTlRfQ09VTlRFUjpcbiAgICAgIHJldHVybiB7IC4uLnN0YXRlLCB2YWx1ZTogc3RhdGUudmFsdWUgLSAxIH1cbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIHsgLi4uc3RhdGUgfVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNvdW50ZXJSZWR1Y2VyXG4iLCJpbXBvcnQge1xuICBVU0VSX0VESVRfUkVRVUVTVCxcbiAgVVNFUl9FRElUX1VQREFURUQsXG59IGZyb20gJy4uL2NvbnN0YW50cy9lZGl0cHJvZmlsZUNvbnN0YW50cydcblxuZXhwb3J0IGNvbnN0IGVkaXRwcm9maWxlUmVkdWNlciA9IChzdGF0ZSA9IHt9LCBhY3Rpb24pID0+IHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgVVNFUl9FRElUX1JFUVVFU1Q6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBsb2FkaW5nOiB0cnVlXG4gICAgICB9XG4gICAgY2FzZSBVU0VSX0VESVRfVVBEQVRFRDpcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGxvYWRpbmc6IGZhbHNlLCAuLi5zdGF0ZSxcbiAgICAgICAgZGF0YVVzZXJFZGl0OiBhY3Rpb24ucGF5bG9hZCxcbiAgICAgIH1cbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIHN0YXRlXG4gIH1cbn1cbiIsImltcG9ydCB7IFVTRVJfSElTVE9SWV9SRVFVRVNUIH0gZnJvbSAnLi4vY29uc3RhbnRzL2hpc3RvcnlDb25zdGFudHMnXG5leHBvcnQgY29uc3QgaGlzdG9yeVJlZHVjZXIgPSAoc3RhdGUgPSB7fSwgYWN0aW9uKSA9PiB7XG4gIHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcbiAgICBjYXNlIFVTRVJfSElTVE9SWV9SRVFVRVNUOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIGhpc3Rvcnk6IGFjdGlvbi5wYXlsb2FkLFxuICAgICAgfVxuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gc3RhdGVcbiAgfVxufVxuIiwiaW1wb3J0IHsgTEVBREVSQk9BUkRfQUNDRVNTIH0gZnJvbSAnLi4vY29uc3RhbnRzL3VzZXJDb25zdGFudHMnXG5cbmNvbnN0IGxlYWRlcmJvYXJkUmVkdWNlciA9IChzdGF0ZSA9IHt9LCBhY3Rpb24pID0+IHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgJ0xFQURFUkJPQVJEX0FDQ0VTUyc6XG4gICAgICByZXR1cm4geyAuLi5zdGF0ZSwgbGVhZGVyOiBhY3Rpb24ucGF5bG9hZCB9XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBzdGF0ZVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGxlYWRlcmJvYXJkUmVkdWNlclxuIiwiaW1wb3J0IHsgTE9HSU5fUkVRVUVTVCB9IGZyb20gJy4uL2NvbnN0YW50cy9sb2dpbkNvbnN0YW50cydcblxuZXhwb3J0IGNvbnN0IGxvZ2luUmVkdWNlciA9IChzdGF0ZSA9IHt9LCBhY3Rpb24pID0+IHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgTE9HSU5fUkVRVUVTVDpcbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLnN0YXRlLFxuICAgICAgICBhY2Nlc3NUb2tlbjogYWN0aW9uLnBheWxvYWQuYWNjZXNzVG9rZW4sXG4gICAgICAgIHJlZnJlc2hUb2tlbjogYWN0aW9uLnBheWxvYWQucmVmcmVzaFRva2VuLFxuICAgICAgfVxuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gc3RhdGVcbiAgfVxufVxuIiwiaW1wb3J0IHtcbiAgVVNFUl9QUk9GSUxFX1JFUVVFU1QsXG4gIFVTRVJfSElTVE9SWV9SRVFVRVNULFxuICBVU0VSX0VESVRfUkVRVUVTVCxcbn0gZnJvbSAnLi4vY29uc3RhbnRzL3Byb2ZpbGVDb25zdGFudHMnXG5cbmV4cG9ydCBjb25zdCBwcm9maWxlUmVkdWNlciA9IChzdGF0ZSA9IHt9LCBhY3Rpb24pID0+IHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgVVNFUl9QUk9GSUxFX1JFUVVFU1Q6XG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5zdGF0ZSxcbiAgICAgICAgcHJvZmlsZTogYWN0aW9uLnBheWxvYWQsXG4gICAgICB9XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBzdGF0ZVxuICB9XG59XG4iLCJpbXBvcnQge1xuICBVU0VSX1JFR0lTVEVSX1JFUVVFU1QsXG4gIFVTRVJfUkVHSVNURVJfU1VDQ0VTUyxcbiAgVVNFUl9SRUdJU1RFUl9GQUlMRUQsXG59IGZyb20gJy4uL2NvbnN0YW50cy91c2VyQ29uc3RhbnRzJ1xuXG5leHBvcnQgY29uc3QgdXNlclJlZ2lzdGVyUmVkdWNlciA9IChzdGF0ZSA9IHt9LCBhY3Rpb24pID0+IHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgVVNFUl9SRUdJU1RFUl9SRVFVRVNUOlxuICAgICAgcmV0dXJuIHsgbG9hZGluZzogdHJ1ZSB9XG4gICAgY2FzZSBVU0VSX1JFR0lTVEVSX1NVQ0NFU1M6XG4gICAgICByZXR1cm4geyBsb2FkaW5nOiBmYWxzZSwgdXNlckluZm86IGFjdGlvbi5wYXlsb2FkIH1cbiAgICBjYXNlIFVTRVJfUkVHSVNURVJfRkFJTEVEOlxuICAgICAgcmV0dXJuIHsgbG9hZGluZzogZmFsc2UsIGVycm9yOiBhY3Rpb24ucGF5bG9hZCB9XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBzdGF0ZVxuICB9XG59XG4iLCJpbXBvcnQgeyBjcmVhdGVTdG9yZSwgY29tYmluZVJlZHVjZXJzLCBhcHBseU1pZGRsZXdhcmUgfSBmcm9tICdyZWR1eCdcbmltcG9ydCB0aHVuayBmcm9tICdyZWR1eC10aHVuaydcbmltcG9ydCB7IGNvbXBvc2VXaXRoRGV2VG9vbHMgfSBmcm9tICdyZWR1eC1kZXZ0b29scy1leHRlbnNpb24nXG5pbXBvcnQgeyBjcmVhdGVXcmFwcGVyIH0gZnJvbSAnbmV4dC1yZWR1eC13cmFwcGVyJ1xuaW1wb3J0IGNvdW50ZXJSZWR1Y2VyIGZyb20gJy4vcmVkdWNlcnMvY291bnRlclJlZHVjZXInXG5pbXBvcnQgeyB1c2VyUmVnaXN0ZXJSZWR1Y2VyIH0gZnJvbSAnLi9yZWR1Y2Vycy91c2VyUmVkdWNlcidcbmltcG9ydCBsZWFkZXJib2FyZFJlZHVjZXIgZnJvbSAnLi9yZWR1Y2Vycy9sZWFkZXJib2FyZFJlZHVjZXInXG5pbXBvcnQgeyBsb2dpblJlZHVjZXIgfSBmcm9tICcuL3JlZHVjZXJzL2xvZ2luUmVkdWNlcidcbmltcG9ydCB7IHByb2ZpbGVSZWR1Y2VyIH0gZnJvbSAnLi9yZWR1Y2Vycy9wcm9maWxlUmVkdWNlcidcbmltcG9ydCB7IGhpc3RvcnlSZWR1Y2VyIH0gZnJvbSAnLi9yZWR1Y2Vycy9oaXN0b3J5UmVkdWNlcidcbmltcG9ydCB7IGVkaXRwcm9maWxlUmVkdWNlciB9IGZyb20gJy4vcmVkdWNlcnMvZWRpdHByb2ZpbGVSZWR1Y2VyJ1xuXG5jb25zdCByZWR1Y2VyID0gY29tYmluZVJlZHVjZXJzKHtcbiAgY291bnRlcjogY291bnRlclJlZHVjZXIsXG4gIHVzZXJSZWdpc3RlcjogdXNlclJlZ2lzdGVyUmVkdWNlcixcbiAgbGVhZGVyQm9hcmQ6IGxlYWRlcmJvYXJkUmVkdWNlcixcbiAgbG9naW46IGxvZ2luUmVkdWNlcixcbiAgcHJvZmlsZVVzZXI6IHByb2ZpbGVSZWR1Y2VyLFxuICBoaXN0b3J5UHJvZmlsZTogaGlzdG9yeVJlZHVjZXIsXG4gIGVkaXRQcm9maWxlOiBlZGl0cHJvZmlsZVJlZHVjZXIsXG59KVxuXG5jb25zdCBpbml0aWFsU3RhdGUgPSB7fVxuXG5jb25zdCBtaWRkbGV3YXJlID0gW3RodW5rXVxuXG5jb25zdCBtYWtlU3RvcmUgPSAoY29udGV4dCkgPT5cbiAgY3JlYXRlU3RvcmUoXG4gICAgcmVkdWNlcixcbiAgICBpbml0aWFsU3RhdGUsXG4gICAgY29tcG9zZVdpdGhEZXZUb29scyhhcHBseU1pZGRsZXdhcmUoLi4ubWlkZGxld2FyZSkpXG4gIClcblxuZXhwb3J0IGNvbnN0IHdyYXBwZXIgPSBjcmVhdGVXcmFwcGVyKG1ha2VTdG9yZSlcbiIsImltcG9ydCBSZWFjdCwgeyBjcmVhdGVDb250ZXh0LCB1c2VTdGF0ZSwgdXNlQ29udGV4dCwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgYXV0aCBmcm9tICcuL2F1dGhTZXJ2aWNlJyA7XHJcblxyXG5jb25zdCBBdXRoQ29udGV4dCA9IGNyZWF0ZUNvbnRleHQoe1xyXG4gIGlzQXV0aGVudGljYXRlZDogZmFsc2UsXHJcbiAgc2V0QXV0aGVudGljYXRlZDogKCkgPT4ge30sXHJcbn0pO1xyXG5cclxuZXhwb3J0IGNvbnN0IEF1dGhQcm92aWRlciA9ICh7IGNoaWxkcmVuIH0pID0+IHtcclxuICBjb25zdCBbaXNBdXRoZW50aWNhdGVkLCBzZXRBdXRoZW50aWNhdGVkXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBbaXNMb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKHRydWUpO1xyXG4gIGNvbnN0IFt1c2VyLCBzZXRVc2VyXSA9IHVzZVN0YXRlKG51bGwpXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGFzeW5jIGZ1bmN0aW9uIGxvYWRGcm9tQXV0aFNlcnZpY2UoKSB7XHJcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBhdXRoLmdldEN1cnJlbnRVc2VyKClcclxuICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgc2V0VXNlcih1c2VyKTtcclxuICAgICAgICAgICAgc2V0QXV0aGVudGljYXRlZCh0cnVlKVxyXG4gICAgICAgIH1cclxuICAgICAgICBzZXRMb2FkaW5nKGZhbHNlKVxyXG4gICAgfVxyXG4gICAgbG9hZEZyb21BdXRoU2VydmljZSgpXHJcbiAgfSwgW10pO1xyXG4gIHJldHVybiAoXHJcbiAgICA8QXV0aENvbnRleHQuUHJvdmlkZXJcclxuICAgICAgdmFsdWU9e3tcclxuICAgICAgICBpc0F1dGhlbnRpY2F0ZWQsXHJcbiAgICAgICAgaXNMb2FkaW5nLFxyXG4gICAgICAgIHNldEF1dGhlbnRpY2F0ZWQsXHJcbiAgICAgICAgdXNlcixcclxuICAgICAgfX1cclxuICAgID5cclxuICAgICAge2NoaWxkcmVufVxyXG4gICAgPC9BdXRoQ29udGV4dC5Qcm92aWRlcj5cclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHVzZUF1dGgoKSB7XHJcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoQXV0aENvbnRleHQpO1xyXG4gIGlmIChjb250ZXh0ID09PSB1bmRlZmluZWQpIHtcclxuICAgIHRocm93IG5ldyBFcnJvcigndXNlQXV0aCBtdXN0IGJlIHVzZWQgd2l0aGluIGFuIEF1dGhQcm92aWRlcicpO1xyXG4gIH1cclxuICByZXR1cm4gY29udGV4dDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHVzZUlzQXV0aGVudGljYXRlZCgpIHtcclxuICBjb25zdCBjb250ZXh0ID0gdXNlQXV0aCgpO1xyXG4gIHJldHVybiBjb250ZXh0LmlzQXV0aGVudGljYXRlZDtcclxufVxyXG4iLCJpbXBvcnQgaHR0cCBmcm9tICcuL2h0dHBTZXJ2aWNlJ1xuaW1wb3J0IGp3dERlY29kZSBmcm9tICdqd3QtZGVjb2RlJ1xuXG5jb25zdCBlbmRQb2ludCA9IGBodHRwczovL3BhY2lmaWMtdGFpZ2EtNTMwNTkuaGVyb2t1YXBwLmNvbS9sb2dpbmBcbmNvbnN0IHRva2VuS2V5ID0gJ2JpbmFyLmFjY2Vzcy50b2tlbidcbmNvbnN0IHJlZnJlc2hLZXkgPSAnYmluYXIucmVmcmVzaC50b2tlbidcblxuLy8gaHR0cC5zZXRKd3QoZ2V0Snd0KCkpXG5cbi8vIGxvZ2luXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gbG9naW4odXNlcm5hbWUsIHBhc3N3b3JkKSB7XG4gIGNvbnN0IHsgZGF0YSB9ID0gYXdhaXQgaHR0cC5wb3N0KGVuZFBvaW50LCB7XG4gICAgdXNlcm5hbWU6IHVzZXJuYW1lLFxuICAgIHBhc3N3b3JkOiBwYXNzd29yZCxcbiAgfSlcbiAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odG9rZW5LZXksIGRhdGEuYWNjZXNzVG9rZW4pXG4gIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHJlZnJlc2hLZXksIGRhdGEucmVmcmVzaFRva2VuKVxufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nb3V0KCkge1xuICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0b2tlbktleSlcbiAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0ocmVmcmVzaEtleSlcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEN1cnJlbnRVc2VyKCkge1xuICB0cnkge1xuICAgIGNvbnN0IHRva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odG9rZW5LZXkpXG4gICAgcmV0dXJuIGp3dERlY29kZSh0b2tlbilcbiAgfSBjYXRjaCAoZXJyKSB7XG4gICAgcmV0dXJuIG51bGxcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Snd0KCkge1xuICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0odG9rZW5LZXkpXG59XG5cbi8vIGV4cG9ydCBmdW5jdGlvbiBnZXROZXdBY2Nlc3NUb2tlbigpIHtcbi8vICAgY29uc3QgcmVmcmVzaCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdyZWZyZXNoVG9rZW4nKVxuLy8gICBjb25zdCByZXF1ZXN0ID0gYXdhaXQgaHR0cC5wb3N0KGNvbmZpZy5hcGlVcmwgKyAncmVmcmVzaC10b2tlbicsIHtcbi8vICAgICByZWZyZXNoVG9rZW46IHJlZnJlc2gsXG4vLyAgIH0pXG4vLyAgIGlmIChyZXF1ZXN0KSB7XG4vLyAgICAgZm9yICh2YXIgayBpbiByZXF1ZXN0KSB7XG4vLyAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShrLCByZXF1ZXN0W2tdKVxuLy8gICAgIH1cbi8vICAgfVxuLy8gfVxuXG5jb25zdCBhdXRoID0ge1xuICBsb2dpbixcbiAgbG9nb3V0LFxuICBnZXRDdXJyZW50VXNlcixcbiAgZ2V0Snd0LFxufVxuXG5leHBvcnQgZGVmYXVsdCBhdXRoXG4iLCJpbXBvcnQgYXhpb3MgZnJvbSBcImF4aW9zXCJcbmltcG9ydCB7IHRvYXN0IH0gZnJvbSBcInJlYWN0LXRvYXN0aWZ5XCJcblxuYXhpb3MuaW50ZXJjZXB0b3JzLnJlc3BvbnNlLnVzZShudWxsLCAoZXJyKSA9PiB7XG4gIGNvbnN0IGV4cGVjdGVkRXJyb3IgPVxuICAgIGVyci5yZXNwb25zZSAmJiBlcnIucmVzcG9uc2Uuc3RhdHVzID49IDQwMCAmJiBlcnIucmVzcG9uc2Uuc3RhdHVzIDwgNTAwXG5cbiAgaWYgKCFleHBlY3RlZEVycm9yKSB7XG4gICAgY29uc29sZS5sb2coXCJMb2dnaW5nIHRoZSB1bmV4cGVjdGVkIGVycm9yXCIsIGVycilcbiAgICB0b2FzdC5lcnJvcihcIkFuIHVuZXhwZWN0ZWQgZXJyb3Igb2NjdXJlZC5cIilcbiAgfVxuXG4gIHJldHVybiBQcm9taXNlLnJlamVjdChlcnIpXG59KVxuXG5mdW5jdGlvbiBzZXRKd3QodG9rZW4pIHtcbiAgYXhpb3MuZGVmYXVsdHMuaGVhZGVycy5jb21tb25bXCJBdXRob3JpemF0aW9uXCJdID0gdG9rZW5cbn1cblxuY29uc3QgaHR0cCA9IHtcbiAgZ2V0OiBheGlvcy5nZXQsXG4gIHBvc3Q6IGF4aW9zLnBvc3QsXG4gIHB1dDogYXhpb3MucHV0LFxuICBkZWxldGU6IGF4aW9zLmRlbGV0ZSxcbiAgc2V0Snd0LFxufVxuXG5leHBvcnQgZGVmYXVsdCBodHRwXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJheGlvc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJqd3QtZGVjb2RlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQtcmVkdXgtd3JhcHBlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC10b2FzdGlmeVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXgtZGV2dG9vbHMtZXh0ZW5zaW9uXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXRodW5rXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=